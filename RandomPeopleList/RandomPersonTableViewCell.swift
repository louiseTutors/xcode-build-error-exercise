//
//  RandomPersonTableViewCell.swift
//  RandomPeopleList
//
//  Created by Louise Chan on 2020-11-05.
//

import UIKit

class RandomPersonTableViewCell: UITableViewCell {
    @IBOutlet weak var personImageView: UIImageView!
    @IBOutlet weak var personLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
