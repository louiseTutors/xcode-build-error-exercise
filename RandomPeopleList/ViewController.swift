//
//  ViewController.swift
//  RandomPeopleList
//
//  Created by Louise Chan on 2020-11-05.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {
    
    private var randomPeople: [Person]! = []
    let peopleDispatchGroup = DispatchGroup()

    @IBOutlet weak var getNewListButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
                
        // Setup button to have rounded corners
        getNewListButton.layer.cornerRadius = 10
        
        // get 10 random people
        getNewListOfPeople()
    }
    
    private func getNewListOfPeople() {
        // get 10 random people
        getRandomPersonData()
        
        DispatchQueue.global(qos: .background).async {
            self.peopleDispatchGroup.wait()
            DispatchQueue.main.async {
                // update table to display the random people
                self.tableView.reloadData()
            }
        }
    }

    // MARK: Get 10 random people from the internet
    private func getRandomPersonData() {
        
        peopleDispatchGroup.enter()
        
        let url = "https://randomuser.me/api/?results=10"
        
        print("url = \(url)")
        AF.request(url).responseJSON {
            (response) in
            
            let data = response.data
            
            do {
                let json = try JSON(data: data!)
                                
                let personArray = json["results"].arrayValue
                
                for person in personArray {
                    
                    let name = person["name"]["first"].stringValue + " " + person["name"]["last"].stringValue
                    let age = person["dob"]["age"].intValue
                    let imageUrl = person["picture"]["medium"].stringValue
                    
                    let randomPerson = Person(name: name, age: age, photoUrl: imageUrl)
                    self.randomPeople.append(randomPerson)
            
                }
                
                self.peopleDispatchGroup.leave()
                                
            } catch {
                print(error.localizedDescription)
            }
            
        }

    }
    @IBAction func getNewListPressed() {
        randomPeople.removeAll()
        getNewListOfPeople()
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return randomPeople.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "randomPersonCell") as! RandomPersonTableViewCell
        
        // Show name and age
        let person = randomPeople[indexPath.row]
        cell.personLabel.text = person.getName() + ", " + String(person.getAge())
        
        // Show photo
        AF.request(person.getPhotoUrl()).responseData {
            response in
            guard let data = response.data else {
                return
            }
            cell.personImageView.image = UIImage(data: data)
        }
        
        return cell
    }
    
    
}

