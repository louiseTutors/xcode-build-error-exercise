//
//  Person.swift
//  RandomPeopleList
//
//  Created by Louise Chan on 2020-11-05.
//

import UIKit
import Foundation

class Person {
    private var name: String
    private var age: Int
    private var photoUrl: String
    
    init(name: String, age: Int, photoUrl: String) {
        self.name = name
        self.age = age
        self.photoUrl = photoUrl
    }
    
    func getPhotoUrl() -> String {
        return photoUrl
    }
    
    func getAge() -> Int {
        return age
    }
    
    func getName() -> String {
        return name
    }
    
    func setName(name: String) {
        self.name = name
    }
    
    func setAge(age: Int) {
        self.age = age
    }
    
    func setPhotoUrl(photoUrl: String) {
        self.photoUrl = photoUrl
    }
    
}
